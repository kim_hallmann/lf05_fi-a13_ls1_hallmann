import java.util.Scanner;
public class Schaltjahr {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.print("Guten Tag. Bitte tragen sie eine Jahreszahl ein, damit ich berechnen kann, ob es sich um ein Schaltjahr handelt:");
		short jahreszahl = myScanner.nextShort();
		myScanner.close();
		
		
		if (jahreszahl >= 1582 && jahreszahl % 4 == 0 && jahreszahl % 100 == 0 && jahreszahl % 400 == 0) {
			System.out.print("Es handelt sich um ein Schaltjahr.");
		}
			
		else if (jahreszahl % 4 == 0 && jahreszahl >= -45 && jahreszahl % 100 != 0 ) {
			System.out.print("Es handelt sich um ein Schaltjahr.");
		}
		else if (jahreszahl < -45) {
			System.out.print("In dieser Zeit gab es noch keine Schaltjahre.");
		}
		else {
			System.out.print("Es handelt sich nicht um ein Schaltjahr.");
		}
		
	}

}
