import java.util.Scanner;

public class FahrkartenKopie{

	 // Preis und Anzahl der Tickets
		public static double fahrkartenbestellungErfassen() {

	    	 // Preis und Anzahl der Tickets
	    	 	  
	    		  Scanner tastatur = new Scanner(System.in);
	    		  int ticketwahl;
	    	 	  double zuZahlenderBetrag = 0;
	    	 	  byte anzahlTickets; 

	    	       

	    	    /*   System.out.println("Regeltarif (AB) f�r 3,00 Euro (1)");
	    	       System.out.println("Regeltarif (BC) f�r 3,50 Euro (2)");
	    	       System.out.println("Regeltarif (ABC) f�r 3,80 Euro (3)");
	    	       System.out.println("Kurzstrecke (AB) f�r 2,00 Euro (4)");
	    	       System.out.println("Tageskarte (AB) f�r 8,80 Euro (5)");
	    	       System.out.println("Tageskarte (BC) f�r 9,20 Euro (6)");
	    	       System.out.println("Tageskarte (ABC) f�r 10,00 Euro (7)");
	    	       System.out.println("Kleingruppen-Tageskarte (AB) f�r 25,50 Euro (8)");
	    	       System.out.println("Kleingruppen-Tageskarte (BC) f�r 26,00 Euro (9)");
	    	       System.out.println("Kleingruppen-Tageskarte (ABC) f�r 26,50 Euro (10)");  */
	    	       

	    			

	    	       	
	    	       String [] tarif = new String [10];
	    	       double [] preis = new double [10];
	    	       
	    	       tarif [0] = "Regeltarif (AB)";
	    	       tarif [1] = "Regeltarif (BC)";
	    	       tarif [2] = "Regeltarif (ABC)";
	    	       tarif [3] = "Kurzstrecke (AB)";
	    	       tarif [4] = "Tageskarte (AB)";
	    	       tarif [5] = "Tageskarte (BC)";
	    	       tarif [6] = "Tageskarte (ABC)";
	    	       tarif [7] = "Kleingruppen-Tageskarte (AB)";
	    	       tarif [8] = "Kleingruppen-Tageskarte (BC)";
	    	       tarif [9] = "Kleingruppen-Tageskarte (ABC)";
	    	       
	    	       
	    	       preis [0] = 3.00;
	    	       preis [1] = 3.50;
	    	       preis [2] = 3.80;
	    	       preis [3] = 2.00;
	    	       preis [4] = 8.80;
	    	       preis [5] = 9.20;
	    	       preis [6] = 10.00;
	    	       preis [7] = 25.50;
	    	       preis [8] = 26.00;
	    	       preis [9] = 26.50;
	    	       
	    	       System.out.println("Sch�nen guten Tag. Bitte w�hlen sie eine Wunschfahrkarte aus:");
	    	       
	    	       for(int i= 0; i <tarif.length; i++) {
	    	    	   System.out.printf(tarif[i]+" f�r ");
	    	    	   System.out.printf("%02.2f", preis[i]);
	    	    	   System.out.printf("  ("+ (i+1) + ")" + "\n");
	    	       }
	    	       ticketwahl = tastatur.nextInt();
	    	       System.out.print("Wie viele Tickets m�chten sie kaufen?: ");
	    	       anzahlTickets = tastatur.nextByte();
	    	       for(int i = 0; i < tarif.length; i++) {
	    	    	   if(i+1 == ticketwahl) {
	    	    	   System.out.println("Sie haben den Tarif:" + tarif[i] + " ausgew�hlt.");
	    	    	   }
	    	       }

	    	       
	    	       for(int j = 0; j < preis.length; j++) {
	    	    	   if (ticketwahl == j +1) {
	    	    	  zuZahlenderBetrag = preis[j] * anzahlTickets;
		   			  System.out.printf("Das ist der Preis: " );
		 	          System.out.printf("%02.2f", zuZahlenderBetrag);
		 	          System.out.printf( " Euro.");
	    	    	   }
	    	       }
	    	       
	    	       
	    	       
	    	       return zuZahlenderBetrag;
		}
      
	// Geldeinwurf
    // -----------
	  public static double fahrkartenBezahlen (double zuZahlen) {
		  
		  Scanner tastatur = new Scanner(System.in);
		  double eingezahlterGesamtbetrag;
		  double eingeworfeneM�nze;
		  double zuZahlenderBetrag = 0.0;
		  zuZahlenderBetrag = zuZahlen;
		  
		  eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f "+ "Euro \n",(zuZahlenderBetrag - eingezahlterGesamtbetrag )); 
	    	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	           
	       }
	       return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}       // Fahrscheinausgabe
      		// -----------------
		  public static void fahrkartenAusgeben(){
		      System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
	 }
		  
	  
	public static double rueckgeldAusgeben(double ausgabe) {
    // R�ckgeldberechnung und -Ausgabe
    // -------------------------------
	
	double r�ckgabebetrag;
	r�ckgabebetrag = ausgabe;
    if(r�ckgabebetrag > 0.0)
    {
 	   System.out.printf("Der R�ckgabebetrag in H�he von ");
 	   System.out.printf("%02.2f", r�ckgabebetrag ); 
 	   System.out.printf(" Euro");
 	   System.out.println(" wird in folgenden M�nzen ausgezahlt:");

        while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
        {
     	  System.out.println("2 EURO");
	          r�ckgabebetrag -= 2.0;
        }
        while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
        {
     	  System.out.println("1 EURO");
	          r�ckgabebetrag -= 1.0;
        }
        while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
        {
     	  System.out.println("50 CENT");
	          r�ckgabebetrag -= 0.5;
        }
        while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
        {
     	  System.out.println("20 CENT");
	          r�ckgabebetrag -= 0.2;
        }
        while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
        {
     	  System.out.println("10 CENT");
	          r�ckgabebetrag -= 0.1;
        }
        while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
        {
     	  System.out.println("5 CENT");
	          r�ckgabebetrag -= 0.05;
        
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir w�nschen Ihnen eine gute Fahrt.");
   
    }
    return ausgabe;
 }
    
    public static void main(String[] args)
    {
    		while (true) {
     	       System.out.println(" ");
    	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	       double rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag);
    	       fahrkartenAusgeben();
    	       rueckgeldAusgeben(rueckgeld);
    		}

    	       
       
    }
}