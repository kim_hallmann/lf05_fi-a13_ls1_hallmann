﻿import java.util.Scanner;

public class Fahrkartenautomat{

	 // Preis und Anzahl der Tickets
		public static double fahrkartenbestellungErfassen() {

	    	 // Preis und Anzahl der Tickets
	    	 	  
	    		  Scanner tastatur = new Scanner(System.in);
	    		  int ticketwahl;
	    	 	  double zuZahlenderBetrag = 0;
	    	 	  byte anzahlTickets; 	
	    	       String [] tarif = new String [10];
	    	       double [] preis = new double [10];
	    	       
	    	       tarif [0] = "Regeltarif (AB)";
	    	       tarif [1] = "Regeltarif (BC)";
	    	       tarif [2] = "Regeltarif (ABC)";
	    	       tarif [3] = "Kurzstrecke (AB)";
	    	       tarif [4] = "Tageskarte (AB)";
	    	       tarif [5] = "Tageskarte (BC)";
	    	       tarif [6] = "Tageskarte (ABC)";
	    	       tarif [7] = "Kleingruppen-Tageskarte (AB)";
	    	       tarif [8] = "Kleingruppen-Tageskarte (BC)";
	    	       tarif [9] = "Kleingruppen-Tageskarte (ABC)";
	    	       
	    	       
	    	       preis [0] = 3.00;			//es lassen sich einfacher Preise bzw. Fahrkartentickets hinzufügen bzw. allgemein ändern indem man Arrays hinzufügt
	    	       preis [1] = 3.50;			//außerdem muss man so nicht immer wieder zB einen Switch Case erstellen sondern kann alles in einem Abschnitt hinzufügen/ entfernen
	    	       preis [2] = 3.80;			//Somit ist das Benutzen von Arrays wesentlich programmierfreundlicher, da man damit weniger Arbeit hat
	    	       preis [3] = 2.00;
	    	       preis [4] = 8.80;
	    	       preis [5] = 9.20;
	    	       preis [6] = 10.00;
	    	       preis [7] = 25.50;
	    	       preis [8] = 26.00;
	    	       preis [9] = 26.50;
	    	       
	    	       System.out.println("Schönen guten Tag. Bitte wählen sie eine Wunschfahrkarte aus:");
	    	       
	    	       for(int i= 0; i <tarif.length; i++) {
	    	    	   System.out.printf(tarif[i]+" für ");
	    	    	   System.out.printf("%02.2f", preis[i]);
	    	    	   System.out.printf("  ("+ (i+1) + ")" + "\n");
	    	       }
	    	       ticketwahl = tastatur.nextInt();
	    	       System.out.print("Wie viele Tickets möchten sie kaufen?: ");
	    	       anzahlTickets = tastatur.nextByte();
	    	       
	    	       for(int i = 0; i < tarif.length; i++) {
	    	    	   if(i+1 == ticketwahl) {
	    	    	   System.out.println("Sie haben den Tarif:" + tarif[i] + " ausgewählt.");
	    	    	   }
	    	       }

	    	       
	    	       for(int j = 0; j < preis.length; j++) {
	    	    	   if (ticketwahl == j +1) {
	    	    	  zuZahlenderBetrag = preis[j] * anzahlTickets;
		   			  System.out.printf("Das ist der Preis: " );
		 	          System.out.printf("%02.2f", zuZahlenderBetrag);
		 	          System.out.printf( " Euro.");
	    	    	   }
	    	       }
	    	       
	    	       
	    	       
	    	       return zuZahlenderBetrag;
		}
      
	// Geldeinwurf
    // -----------
	  public static double fahrkartenBezahlen (double zuZahlen) {
		  
		  Scanner tastatur = new Scanner(System.in);
		  double eingezahlterGesamtbetrag;
		  double eingeworfeneMünze;
		  double zuZahlenderBetrag = 0.0;
		  zuZahlenderBetrag = zuZahlen;
		  
		  eingezahlterGesamtbetrag = 0.0;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.printf("Noch zu zahlen: " + "%.2f "+ "Euro \n",(zuZahlenderBetrag - eingezahlterGesamtbetrag )); 
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
	    	   eingeworfeneMünze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneMünze;
	           
	       }
	       return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}       // Fahrscheinausgabe
      		// -----------------
		  public static void fahrkartenAusgeben(){
		      System.out.println("\nFahrschein wird ausgegeben");
		       for (int i = 0; i < 8; i++)
		       {
		          System.out.print("=");
		          try {
					Thread.sleep(250);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       }
		       System.out.println("\n\n");
	 }
		  
	  
	public static double rueckgeldAusgeben(double ausgabe) {
    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
	
	double rückgabebetrag;
	rückgabebetrag = ausgabe;
    if(rückgabebetrag > 0.0)
    {
 	   System.out.printf("Der Rückgabebetrag in Höhe von ");
 	   System.out.printf("%02.2f", rückgabebetrag ); 
 	   System.out.printf(" Euro");
 	   System.out.println(" wird in folgenden Münzen ausgezahlt:");

        while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
        {
     	  System.out.println("2 EURO");
	          rückgabebetrag -= 2.0;
        }
        while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
        {
     	  System.out.println("1 EURO");
	          rückgabebetrag -= 1.0;
        }
        while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
        {
     	  System.out.println("50 CENT");
	          rückgabebetrag -= 0.5;
        }
        while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
        {
     	  System.out.println("20 CENT");
	          rückgabebetrag -= 0.2;
        }
        while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
        {
     	  System.out.println("10 CENT");
	          rückgabebetrag -= 0.1;
        }
        while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
        {
     	  System.out.println("5 CENT");
	          rückgabebetrag -= 0.05;
        
    }

    System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                       "vor Fahrtantritt entwerten zu lassen!\n"+
                       "Wir wünschen Ihnen eine gute Fahrt.");
   
    }
    return ausgabe;
 }
    
    public static void main(String[] args)
    {
    		while (true) {
     	       System.out.println(" ");
    	       double zuZahlenderBetrag = fahrkartenbestellungErfassen();
    	       double rueckgeld = fahrkartenBezahlen(zuZahlenderBetrag);
    	       fahrkartenAusgeben();
    	       rueckgeldAusgeben(rueckgeld);
    		}

    	       
       
    }
}